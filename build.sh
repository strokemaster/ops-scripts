#!/bin/bash
source $SCRIPTS_HOME/build.properties


# Echo some debug info at the beginning of the script
echo ""; echo "";
java -version; echo ""
echo "Workspace: $WORKSPACE";echo ""

# If this variable is not set, then tbe setup is not done properly
if [[ -z "$ARCHIVE" ]]; then
        echo "ARCHIVE not set, failing build"
	exit 1
# If the archive directory doesn't exist, create it
elif [[ ! -d $ARCHIVE ]]; then
	mkdir $ARCHIVE
fi

# Set the branch based on the Git plugin value
BRANCH=${GIT_BRANCH#origin/}

# Go the workspace, run the maven build and store the exit code
cd $WORKSPACE
$M2_HOME/bin/mvn clean install
EXIT_CODE=$?

# If the exit code is not 0, the build fucked up
if [[ $EXIT_CODE -ne 0 ]]; then
	echo "Maven Build failed, exiting script"
	exit $EXIT_CODE
fi

# Check to see if BRANCH folder is created
if [[ ! -d $ARCHIVE/strokemaster ]]; then
	mkdir $ARCHIVE/strokemaster
fi

# Check to see if BRANCH folder is created
if [[ ! -d $ARCHIVE/strokemaster/$BRANCH ]]; then
	mkdir $ARCHIVE/strokemaster/$BRANCH
fi


# Check the number of artifacts in the current BRANCH
if [[ ! $BRANCH = "master" ]]; then
	NUM_ARTIFACTS=`ls -l $ARCHIVE/strokemaster/$BRANCH | wc -l`
	if [[ "$NUM_ARTIFACTS" -gt 10 ]]; then
		OLDEST=`ls -tr1|head -1`
		if [ -z $OLDEST ] || [ $OLDEST = "" ]; then
			echo "OLDEST not defined"
		else
			echo "OLDEST and ARCHIVE are not defined" 
			#rm -rf $ARCHIVE/
		fi
	fi
fi

# Archive the jar file
mkdir $ARCHIVE/strokemaster/$BRANCH/strokemaster-$BRANCH-$BUILD_NUMBER
cp $WORKSPACE/target/strokemaster-api.jar $ARCHIVE/strokemaster/$BRANCH/strokemaster-$BRANCH-$BUILD_NUMBER

# Archive the config.yml file
cp $WORKSPACE/src/main/config/*config.yml $ARCHIVE/strokemaster/$BRANCH/strokemaster-$BRANCH-$BUILD_NUMBER
