#!/bin/bash
# This script is used for connecting to the database and executing a sql file
source $SCRIPTS_HOME/db_$Env.properties

# Jenkins Input
# The environment the script is pointing to.  Determines connection setings
FILE=$1

# Connect to the database
mysql -h $DB_URL -P $DB_PORT -u $DB_USER -p$DB_PASSWORD $DB_NAME -e "source $FILE" > output.txt
cat output.txt
rm output.txt
echo ""
exit 0 
