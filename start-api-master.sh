#!/bin/bash
# This script is used for getting a version of the API started up
# This will be just pulling the latest version of master for now
source $SCRIPTS_HOME/build.properties

# Jenkins Input
#VERSION=$VERSION
#BRANCH=$BRANCH

# End Jenkins Input

ENV="dev"
LATEST=`ls -t $ARCHIVE/strokemaster/master/ | head -1`
echo "Latest: $LATEST"

# Remove the previous artifact if it exists
if [[ -f $TEST_DIR/strokemaster-api.jar ]]; then
	rm $TEST_DIR/strokemaster-api.jar
fi 


# Remove the previous config.yml if it exists
if [[ -f $TEST_DIR/config.yml ]]; then
	rm $TEST_DIR/config.yml
fi 

# Remove the previous log file if it exists
if [[ -f $TEST_DIR/log.txt ]]; then
	rm $TEST_DIR/log.txt
fi 

# Remove the previous log file if it exists
if [[ -f $TEST_DIR/nohup.out ]]; then
	rm $TEST_DIR/nohup.out
fi 
# Copy the artifact and config file to the test directory
cp $ARCHIVE/strokemaster/master/$LATEST/* $TEST_DIR

# Check to see if the artifact was copied correctly
if [[ ! -f $TEST_DIR/strokemaster-api.jar ]]; then
	echo "JAR does not exist, deploy failed"
	exit 1
fi 

# If the ec2-config-yml file exists, use that instead of default
if [[ -f $TEST_DIR/$ENV-config.yml ]]; then
	rm $TEST_DIR/config.yml
	mv $TEST_DIR/$ENV-config.yml $TEST_DIR/config.yml
fi

# Check to see if the config.yml file was copied correctly
if [[ ! -f $TEST_DIR/config.yml ]]; then
	echo "Config file does not exist, deployed failed"
	exit 1
fi 

echo ""
cd $TEST_DIR
echo "Test Directory Contents"
ls -ltr 
echo ""

# Start the server process 
#sudo service strokemaster restart
BUILD_ID=dontKillMe sudo service strokemaster restart


# Grab the response code 
responseCode=`curl -s -o /dev/null -w "%{http_code}" http://localhost:9000/hello-world`

if [[ $responseCode = *200* ]]; then
	echo "Application started successfully"
	exit 0
else
	echo "Application startup failed"
	exit 1
fi
